﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Boxinator.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Account",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    DateOfBirth = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    ContactNumber = table.Column<string>(type: "nvarchar(25)", maxLength: 25, nullable: true),
                    Country = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    PostalCode = table.Column<string>(type: "nvarchar(18)", maxLength: 18, nullable: true),
                    AccountType = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Account", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Country",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CountryName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CountryMultiplier = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Country", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TwoFactor",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    SixDigitCode = table.Column<int>(type: "int", nullable: false),
                    TimeStamp = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TwoFactor", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Shipment",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ReceiverName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    DestinationCountry = table.Column<int>(type: "int", nullable: false),
                    Weight_kg = table.Column<int>(type: "int", nullable: false),
                    BoxColour = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Price = table.Column<double>(type: "float", nullable: false),
                    Date = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AccountId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shipment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Shipment_Account_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Account",
                columns: new[] { "Id", "AccountType", "ContactNumber", "Country", "DateOfBirth", "Email", "FirstName", "LastName", "PostalCode" },
                values: new object[,]
                {
                    { 1, 2, null, "France", null, "benjaminandersen94@gmail.com", "Benjamin", "Andersen", null },
                    { 2, 2, null, "Norway", null, "emblaoye@gmail.com", "Embla", "Øye", null },
                    { 3, 2, null, "Russia", null, "stian.kvamme98@gmail.com", "Stian", "Kvamme", null }
                });

            migrationBuilder.InsertData(
                table: "Country",
                columns: new[] { "Id", "CountryMultiplier", "CountryName" },
                values: new object[,]
                {
                    { 22, 6.0, "Hungary" },
                    { 23, 6.0, "Bosnia and Herzegovina" },
                    { 24, 6.5, "Serbia" },
                    { 25, 6.5, "Romania" },
                    { 26, 7.0, "Bulgaria" },
                    { 27, 7.0, "Greece" },
                    { 28, 7.5, "Turkey" },
                    { 29, 6.5, "Montenegro" },
                    { 30, 7.0, "Albania" },
                    { 31, 7.0, "North Macedonia" },
                    { 32, 7.0, "Kosovo" },
                    { 33, 7.0, "Moldova" },
                    { 34, 7.0, "Portugal" },
                    { 35, 6.5, "Ukraine" },
                    { 36, 6.0, "Belarus" },
                    { 37, 6.0, "Lithuania" },
                    { 21, 6.0, "Croatia" },
                    { 20, 6.0, "Slovenia" },
                    { 18, 5.5, "Austria" },
                    { 38, 6.0, "Latvia" },
                    { 1, 1.0, "Norway" },
                    { 2, 1.0, "Sweden" },
                    { 3, 1.0, "Denmark" },
                    { 4, 5.0, "Germany" },
                    { 5, 6.0, "France" },
                    { 6, 7.0, "Spain" },
                    { 7, 5.5, "Belgium" },
                    { 8, 5.0, "Netherlands" },
                    { 9, 5.0, "Poland" },
                    { 10, 4.0, "United Kingdom" },
                    { 11, 4.5, "Ireland" },
                    { 12, 2.0, "Finland" },
                    { 13, 3.0, "Iceland" },
                    { 14, 7.0, "Russia" },
                    { 15, 6.5, "Italy" },
                    { 16, 5.5, "Czechia" },
                    { 17, 5.5, "Slovakia" },
                    { 19, 5.5, "Switzerland" },
                    { 39, 6.0, "Estonia" }
                });

            migrationBuilder.InsertData(
                table: "Shipment",
                columns: new[] { "Id", "AccountId", "BoxColour", "Date", "DestinationCountry", "Price", "ReceiverName", "Status", "Weight_kg" },
                values: new object[,]
                {
                    { 6, 1, "rgba(250,128,114,1)", "25/10/2021 15:17", 5, 248.0, "Benjamin Andersen", 3, 8 },
                    { 1, 2, "rgba(250,128,114,1)", "25/10/2021 15:17", 1, 205.0, "Embla Øye", 2, 5 },
                    { 2, 2, "rgba(250,128,114,1)", "25/10/2021 15:17", 1, 208.0, "Embla Øye", 3, 8 },
                    { 3, 2, "rgba(250,128,114,1)", "25/10/2021 15:17", 1, 202.0, "Embla Øye", 0, 2 },
                    { 4, 2, "rgba(250,128,114,1)", "25/10/2021 15:17", 1, 205.0, "Embla Øye", 1, 5 },
                    { 5, 3, "rgba(2,255,0,0.5)", "25/10/2021 15:17", 14, 214.0, "Stian Kvamme", 4, 2 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Shipment_AccountId",
                table: "Shipment",
                column: "AccountId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Country");

            migrationBuilder.DropTable(
                name: "Shipment");

            migrationBuilder.DropTable(
                name: "TwoFactor");

            migrationBuilder.DropTable(
                name: "Account");
        }
    }
}
