﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Models.Domain
{
    [Table("TwoFactor")]
    public class TwoFactor
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int SixDigitCode { get; set; }
        public DateTime TimeStamp { get; set; }
        public TwoFactor()
        {
            TimeStamp = DateTime.UtcNow.AddMinutes(6);
        }
    }
}
