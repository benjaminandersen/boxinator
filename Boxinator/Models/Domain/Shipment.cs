﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Models.Domain
{
    [Table("Shipment")]
    public class Shipment
    {
        // Primary Key
        public int Id { get; set; }

        // Fields
        [Required]
        [MaxLength(200)]
        public string ReceiverName { get; set; }

        [Required]
        public StatusType Status { get; set; }

        [Required]
        public int DestinationCountry { get; set; }

        [Required]
        public int Weight_kg { get; set; }

        [Required]
        [MaxLength(50)]
        public string BoxColour { get; set; }

        [Required]
        public double Price { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy HH:mm}")]
        public string Date { get; set; }

        public Shipment()
        {
            Date = DateTime.Now.ToString("dd'/'MM'/'yyyy HH:mm");
        }

        // Relationships
        public int? AccountId { get; set; }
        public Account? Account { get; set; }
    }
}
