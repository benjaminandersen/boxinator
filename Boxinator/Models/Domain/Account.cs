﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Models.Domain
{
    [Table("Account")]
    public class Account
    {
        // Primary Key
        public int Id { get; set; }

        // Fields 
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(200)]
        public string Email { get; set; }

        [MaxLength(10)]
        // maybe use DateTime?
        public string DateOfBirth { get; set; }

        [MaxLength(25)]
        public string ContactNumber { get; set; }

        [MaxLength(50)]
        public string Country { get; set; }

        [MaxLength(18)]
        public string PostalCode { get; set; }

        public AccountType AccountType { get; set; }


        // Relationships
        public ICollection<Shipment> Shipments { get; set; }
    }
}
