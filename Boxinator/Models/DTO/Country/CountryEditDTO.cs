﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Models.DTO.Country
{
    public class CountryEditDTO
    {
        public int Id { get; set; }
        public string CountryName { get; set; }
        public double CountryMultiplier { get; set; }
    }
}
