﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Models.DTO.Country
{
    public class CountryCreateDTO
    {
        public string CountryName { get; set; }
        public double CountryMultiplier { get; set; }
    }
}
