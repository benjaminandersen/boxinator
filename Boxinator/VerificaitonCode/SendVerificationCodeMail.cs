﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Boxinator.Mailtemplate;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Boxinator.VerificationCode
{
    public class SendVerificationCodeMail
    {
        private readonly IConfiguration Configuration;
        public SendVerificationCodeMail(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public async Task SendCodeMail(string code, string email, string fname, string lname)
        {
            //bind object model from configuration
            MyConfig conf = Configuration.GetSection(MyConfig.MailValues).Get<MyConfig>();
            var apiKey = conf.MailKey.ToString();//MailKey from appsettings.json;

            //makes html file string 
            // comment in the line below to run locally
            //string FilePath = "Mailtemplate\\VerificationCode.html";
            string FilePath = "Mailtemplate/VerificationCode.html";
            StreamReader str = new StreamReader(FilePath);
            string MailText = str.ReadToEnd();
            str.Close();

            ////sets parameters in html string
            string mailSend = MailText.Replace("%%verificationcode", code);

            MailMessage message = new MailMessage();
            message.IsBodyHtml = true;
            message.Subject = "Verification Code: ";
            message.To.Add(email);
            message.Body = mailSend;
            message.From = new MailAddress("benjamin.andersen@no.experis.com", "Boxinator Creators");

            SmtpClient client2 = new SmtpClient("smtp.sendgrid.net");
            client2.UseDefaultCredentials = false;
            client2.Credentials = new NetworkCredential("apikey", apiKey);
            client2.DeliveryMethod = SmtpDeliveryMethod.Network;
            client2.Port = 587;
            client2.Timeout = 99999;
            client2.EnableSsl = true;

            await client2.SendMailAsync(message);
        }
    }
}
