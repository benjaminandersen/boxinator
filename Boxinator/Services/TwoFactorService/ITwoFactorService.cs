﻿using Boxinator.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Services.TwoFactorService
{
    public interface ITwoFactorService
    {
        public Task<TwoFactor> GetTwoFactorAsync(int id);
        public Task<TwoFactor> AddTwoFactorAsync(TwoFactor twoFactor);
        public Task DeleteTwoFactorAsync(int id);
        public bool TwoFactorCodeExists(int userId);
    }
}
