﻿using Boxinator.Models;
using Boxinator.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Boxinator.Services.AccountService
{
    public class AccountService : IAccountService
    {
        private readonly BoxinatorDbContext _context;

        public AccountService(BoxinatorDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Account>> GetAllAccountsAsync()
        {
            return await _context.Accounts
                .Include(a => a.Shipments)
                .ToListAsync();
        }

        public async Task<Account> GetAccountAsync(int id)
        {
            return await _context.Accounts
                .Include(a => a.Shipments)
                .Where(a => a.Id == id)
                .FirstAsync();
        }

        public async Task<Account> GetAccountFromEmailAsync(string email)
        {
            return await _context.Accounts
                .Include(a => a.Shipments)
                .Where(a => a.Email == email)
                .FirstAsync();
        }

        public async Task UpdateAccountAsync(Account account)
        {
            _context.Entry(account).State = EntityState.Modified;
            _context.Entry(account)
                .Property(a => a.AccountType).IsModified = false;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAdminAccountAsync(Account account)
        {
            _context.Entry(account).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task<Account> AddAccountAsync(Account account)
        {
            _context.Accounts.Add(account);
            await _context.SaveChangesAsync();
            return account;
        }

        public async Task DeleteAccountAsync(int id)
        {
            var account = await _context.Accounts.FindAsync(id);
            _context.Accounts.Remove(account);
            await _context.SaveChangesAsync();
        }

        public bool AccountExists(int id)
        {
            return _context.Accounts.Any(a => a.Id == id);
        }

        public bool AccountEmailExists(string email)
        {
            return _context.Accounts.Any(a => a.Email == email);
        }
    }
}
